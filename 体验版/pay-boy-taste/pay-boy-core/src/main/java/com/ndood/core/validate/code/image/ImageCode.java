package com.ndood.core.validate.code.image;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

import com.ndood.core.validate.code.ValidateCode;

/**
 * 创建验证码类
 * @author ndood
 */
public class ImageCode extends ValidateCode{
	private static final long serialVersionUID = 693250056555554638L;
	
	/**
	 * 验证码图片
	 */
	private BufferedImage image;

	/**
	 * 构造方法，过期时间点
	 */
	public ImageCode(BufferedImage image, String code, int expireIn) {
		super(code, expireIn);
		this.image = image;
	}
	
	public ImageCode(BufferedImage image, String code, LocalDateTime expireTime) {
		super(code, expireTime);
		this.image = image;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}
}
