package com.ndood.web.admin.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailSenderTest {

	@Autowired
	JavaMailSender jms;

	String activate_template = "尊敬的%s，您好！<br>"+
	"<br>"+
	"<br>"+
    "点击链接即可激活您的payboyplus账号,<br>"+
    "<br>"+
    "<a href=\"%s%s?active=%s&linkid=%s\" rel=\"noopener\" target=\"_blank\">"+ 
    "%s%s?active=%s&linkid=%s"+ 
    "</a><br>"+
    "<br>"+
    "为保障您的帐号安全，请在24小时内点击该链接，您也可以将链接复制到浏览器地址栏访问。如果您并未尝试激活邮箱，请忽略本邮件，由此给您带来的不便请谅解。<br>"+
    "<br>"+
    "<br>"+
    "本邮件由系统自动发出，请勿直接回复！";
	
	@Test
	public void send() throws MessagingException{
		
		/*//建立邮件消息
		SimpleMailMessage mainMessage = new SimpleMailMessage();
		//发送者
		mainMessage.setFrom("payboyplus@126.com");
		//接收者
		mainMessage.setTo("908304389@qq.com");
		//发送的标题
		mainMessage.setSubject("这是一封测试邮件");
		//发送的内容
		mainMessage.setText("今天天气不错: )");
		jms.send(mainMessage);*/
		
		String from = "payboyplus@126.com";
		String sendTo = "908304389@qq.com";
		String subject = "这是一封测试激活邮件！";
		String username = "908304389@qq.com";
		String domain = "http://www.payboy.top";
		String activateUrl = "/user/regist/email_finish";
		String activeCode = "asdfadfasdadfsa";
		String linkedId = "123";
		
		//建立邮件消息
		MimeMessage mimeMessage = jms.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		//发送者
		helper.setFrom(from);
		//接收者
		helper.setTo(sendTo);
		//发送的标题
		helper.setSubject(subject);
		//发送的内容
		String text = String.format(activate_template, username, domain, activateUrl, activeCode, linkedId, domain, activateUrl, activeCode, linkedId);
		helper.setText(text,true);
		// 发送邮件
		jms.send(mimeMessage);
		
	}
	
}