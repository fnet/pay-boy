define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'layui',
	'jquery-treetable',
	'jquery-treetable-extension',
],function(){
	return {
		initUpdateDialog: function() {
			// 表单验证
			$("#department_update_form").validate({
				rules: {
				},
				messages:{
				},
			    errorPlacement: function(error,element) {
			    	$(element).parent().updateClass('has-error');
			        $(element).after(error);
			    },
				success:function(element) {
			        $(element).parent().removeClass('has-error');
			        $(element).parent().find('label').remove();
			    },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,	
			});
		},
		initAddDialog: function (){
			// 表单验证
			$("#department_add_form").validate({
				rules: {
				},
				messages:{
				},
			    errorPlacement: function(error,element) {
			    	$(element).parent().addClass('has-error');
			        $(element).after(error);
			    },
				success:function(element) {
			        $(element).parent().removeClass('has-error');
			        $(element).parent().find('label').remove();
			    },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,	
			});
		},
		initPage: function(){
			// 初始化表单控件
			$("#department_treetable").ndoodTreeTable({
				id: "id",  
			    parentId: "parentId",  
				expandIndex : 3, // 根据第4列展开
				sortable : true, // 是否前端排序
				sortIndex : 5, // 根据第6列排序
				type: "GET",
			    url: "/system/department/treeview",
				columns: [
					{
						field:'id',
						title:'',
						width:'50px',
						align:'center',
						formatter:function(value,row){
							return '<input type="checkbox" value="'+value+'">'
						}
					},
					{
						field:'id',
						title:'ID',
						width:'80px',
						align:'center'
					},
					{
						field:'parentId',
						title:'父节点ID',
						width:'80px',
						align:'center',
						formatter:function(value,row){
							if(value==null | value==undefined){
								return 0;
							}
							return value;
						}
					},
					{
						field:'name',
						title: '部门名称'
					},
					{
						field:'status',
						title: '状态',
						formatter:function(value,row){
							if(value==0){
								return '<span class="text-danger"><i class="fa fa-circle"></i> 禁用</span>';
							}else{
								return '<span class="text-success"><i class="fa fa-circle"></i> 启用</span>';
							}
						}
					},
					{
						field:'sort',
						title: '排序'
					},
					{
						field:'createTime',
						width:'200px',
						title: '创建时间'
					},
					{
						field:'updateTime',
						width:'200px',
						title: '修改时间'
					},
					{
						field:'id',
						title:'操作',
						width:'120px',
						formatter:function(value,row){
							var add = '<a href="javascript:;" class="btn btn-xs btn-success btn-delone" onclick="department_add_sub('+row.id+',\''+row.name+'\')"><i class="fa fa-plus"></i></a> ';
							var mod = '<a href="javascript:;" class="btn btn-xs btn-success btn-editone" onclick="department_update('+row.id+')"><i class="fa fa-pencil"></i></a> ';
							var del = '<a href="javascript:;" class="btn btn-xs btn-success btn-delone" onclick="department_delete('+row.id+')"><i class="fa fa-trash"></i></a> ';
							return add+mod+del;
						}
					}
				] 
			});
			
			// 初始化选中事件
			$("#department_treetable").on("click","tbody>tr",function(e){
				if(e.target.localName == "a"|e.target.localName == "i"){ 
					return;
				}
				var id = $(this).attr("data-node-id");
				if($(this).hasClass("selected")){
					// 如果选中则反选
					$(this).removeClass("selected");
					$(this).find("input[type='checkbox']").removeAttr("checked");
					return;
				}
				// 如果反选则选中
				$(this).addClass("selected");
				$(this).find("input[type='checkbox']").prop("checked", true);
			});
			
		}
	}
});

//保存地区
function add_department(callback){
	// Step1: 表单验证
	var valid = $("#department_add_form").valid();
	if(!valid){
		return;
	}
	
	// Step2: 获取输入数据，拼装成请求数据
	var data = $('#department_add_form').serializeJson();
	
	// Step3: 提交表单
    $.ajax({
    	url:"/system/department/add",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:false,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 2});
        		var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
    			parent.layer.close(index);
        		return;
        	}
        	parent.layer.alert(data.msg, {icon: 1});
        	var row = data.data;
        	var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
			parent.layer.close(index);
        	callback(row);
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}

//保存部门
function update_department(callback){
	// Step1: 表单验证
	var valid = $("#department_update_form").valid();
	if(!valid){
		return;
	}
	
	// Step2: 获取输入数据，拼装成请求数据
	var data = $('#department_update_form').serializeJson();
	
	// Step3: 提交表单
    $.ajax({
    	url:"/system/department/update",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:false,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 2});
        		var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
    			parent.layer.close(index);
        		return;
        	}
        	parent.layer.alert(data.msg, {icon: 1});
        	var row = data.data;
        	var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
			parent.layer.close(index);
        	callback(row);
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}

//添加根节点
function department_add_primary(id){
	var docW = window.innerWidth;
	var area = [ '100%', '100%' ];
	var maxmin = false;
	if (docW >= 768) {
		area = [ '800px', '400px' ];
		maxmin = true;
	}
	var i = layer.open({
		type : 2,
		title : '添加部门',
		maxmin : maxmin,
		shadeClose : false,
		area : area,
		content : '/system/department/add',
		btn : [ '保存+btn-success+fa-check', '取消+btn-success+fa-close' ],
		btn1 : function(index) {
			var loadindex = layer.load(1, {time: 5*1000});	
			// 点击确定关闭对话框，并更新数据
			$("#layui-layer-iframe" + i)[0].contentWindow.add_department(function(row){
				if(row!=null){
					$("#department_treetable").ndoodTreeTable("addBranch", row);
				}
			});
			layer.close(loadindex);
		},
		btn2 : function(index) {
			layer.close(i);
		}
	});
	if (docW >= 768) {
		layer.restore(i);
	}else{
		layer.full(i);
	}
}

// 添加子节点
function department_add_sub(id,name){
	var docW = window.innerWidth;
	var area = [ '100%', '100%' ];
	var maxmin = false;
	if (docW >= 768) {
		area = [ '800px', '400px' ];
		maxmin = true;
	}
	var i = layer.open({
		type : 2,
		title : '添加部门',
		maxmin : maxmin,
		shadeClose : false,
		area : area,
		content : '/system/department/add?parentId='+id+'&parentName='+name,
		btn : [ '保存+btn-success+fa-check', '取消+btn-success+fa-close' ],
		btn1 : function(index) {
			var loadindex = layer.load(1, {time: 5*1000});	
			// 点击确定关闭对话框，并更新数据
			$("#layui-layer-iframe" + i)[0].contentWindow.add_department(function(row){
				if(row!=null){
					$("#department_treetable").ndoodTreeTable("addBranch", row);
				}
			});
			layer.close(loadindex);
		},
		btn2 : function(index) {
			layer.close(i);
		}
	});
	if (docW >= 768) {
		layer.restore(i);
	}else{
		layer.full(i);
	}
}

// 修改节点内容
function department_update(id){
	var docW = window.innerWidth;
	var area = [ '100%', '100%' ];
	var maxmin = false;
	if (docW >= 768) {
		area = [ '800px', '400px' ];
		maxmin = true;
	}
	var i = layer.open({
		type : 2,
		title : '修改部门',
		maxmin : maxmin,
		shadeClose : false,
		area : area,
		content : '/system/department/update?id='+id,
		btn : [ '保存+btn-success+fa-check', '取消+btn-success+fa-close' ],
		btn1 : function(index) {
			var loadindex = layer.load(1, {time: 5*1000});	
			// 点击确定关闭对话框，并更新数据
			$("#layui-layer-iframe" + i)[0].contentWindow.update_department(function(row){
				if(row!=null){
					$("#department_treetable").ndoodTreeTable("modifyNode", row);
				}
			});
			layer.close(loadindex);
		},
		btn2 : function(index) {
			layer.close(i);
		}
	});
	if (docW >= 768) {
		layer.restore(i);
	}else{
		layer.full(i);
	}
}

// 批量删除节点
function department_batch_delete(){
	var ids = [];
	var hasSubNodes = false;
	$('#department_treetable').find("input[type='checkbox']:checked").each(function(i, tr){
		var id = $(tr).val();
		var has = $("#department_treetable").ndoodTreeTable("hasSubNodes", id);
		if(has){
			hasSubNodes = true;
		}
		ids.push(parseInt($(tr).val()));
	}); 
	if(hasSubNodes){
		layer.alert('节点下面有子节点，无法删除!',{icon: 2});
		return;
	}
	if (ids.length == 0) {
		layer.alert("请至少选择一条记录!",{icon: 7});
		return;
	}
	layer.confirm("确定要删除选中的'" + ids.length + "'条记录吗?", {
		btn : [ '确定', '取消' ]
	}, function() {
		$.ajax({
			type : 'post',
			data : {ids:ids},
			dataType : 'json',
			url : '/system/department/batch_delete',
			success : function(data) {
				if (data.code != '10000') {
					layer.alert(data.msg,{icon: 2});
					return;
				}
				for(var i=0;i<ids.length;i++){
					$("#department_treetable").ndoodTreeTable("removeNode", ids[i]);
				}
				layer.alert(data.msg, {icon: 1});
			},
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
		});
	}, function() {});
}

// 删除节点
function department_delete(id){
	// Step1: 如果节点下面有子节点，则无法删除
	var hasSubNodes = $("#department_treetable").ndoodTreeTable("hasSubNodes", id);
	if(hasSubNodes){
		layer.alert('该节点下面有子节点，无法删除!',{icon: 2});
		return;
	}
	layer.confirm('确定要删除吗？', {
		btn : [ '确定', '取消' ]
	}, function() {
		// Step2: 调用异步请求并删除节点
		$.ajax({
			url : '/system/department/delete',
			type : "post",
			data : {'id' : id},
			success : function(data) {
				if (data.code != '10000') {
					layer.alert(data.msg,{icon: 2});
					return;
				}
				$("#department_treetable").ndoodTreeTable("removeNode", id);
				layer.alert(data.msg, {icon: 1});
			},
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
		});
	});
}

//展开关闭
function department_expand_all(obj){
	if($(obj).find('i').hasClass('fa-plus-square')){
		$("#department_treetable").ndoodTreeTable("expandAll");
		$(obj).text('全部关闭');
		$(obj).prepend($('<i aria-hidden="true" class="fa fa-minus-square"></i>'));
		return;
	}
	$("#department_treetable").ndoodTreeTable("collapseAll");
	$(obj).text('全部展开');
	$(obj).prepend($('<i aria-hidden="true" class="fa fa-plus-square"></i>'));
}