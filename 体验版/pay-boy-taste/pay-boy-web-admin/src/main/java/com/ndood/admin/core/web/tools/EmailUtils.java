package com.ndood.admin.core.web.tools;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailUtils {

	String activate_template = "尊敬的%s，您好！<br>"+
	"<br>"+
	"<br>"+
    "点击链接即可激活您的payboyplus账号,<br>"+
    "<br>"+
    "<a href=\"%s%s?active=%s&linkid=%s\" rel=\"noopener\" target=\"_blank\">"+ 
    "%s%s?active=%s&linkid=%s"+ 
    "</a><br>"+
    "<br>"+
    "为保障您的帐号安全，请在24小时内点击该链接，您也可以将链接复制到浏览器地址栏访问。如果您并未尝试激活邮箱，请忽略本邮件，由此给您带来的不便请谅解。<br>"+
    "<br>"+
    "<br>"+
    "本邮件由系统自动发出，请勿直接回复！";

	@Autowired
	JavaMailSender jms;
	
	/**
	 * 发送激活邮件
	 */
	public void sendActivationEmail(String sendTo, String from, String subject, String username, String domain, String activateUrl, String activeCode, String linkedId) throws MessagingException{
		
		//建立邮件消息
		MimeMessage mimeMessage = jms.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		//发送者
		helper.setFrom(from);
		//接收者
		helper.setTo(sendTo);
		//发送的标题
		helper.setSubject(subject);
		//发送的内容
		String text = String.format(activate_template, username, domain, activateUrl, activeCode, linkedId, domain, activateUrl, activeCode, linkedId);
		helper.setText(text, true);
		// 发送邮件
		jms.send(mimeMessage);
		
	}

	String change_pwd_template = "尊敬的pay-boy用户，您好！<br>"+
	"<br>"+
	"<br>"+
    "您在访问pay-boy时点击了“忘记密码”链接，这是一封密码重置确认邮件。<br>"+
    "<br>"+
    "您可以通过点击以下链接重置账户密码:"+
	"<br>"+
    "<a href=\"%s%s?active=%s&linkid=%s\" rel=\"noopener\" target=\"_blank\">"+ 
    "%s%s?active=%s&linkid=%s"+ 
    "</a><br>"+
    "<br>"+
    "为保障您的帐号安全，请在24小时内点击该链接，您也可以将链接复制到浏览器地址栏访问。如果您并未尝试激活邮箱，请忽略本邮件，由此给您带来的不便请谅解。<br>"+
    "<br>"+
    "<br>"+
    "本邮件由系统自动发出，请勿直接回复！";
	
	/**
	 * 发送修改密码邮件
	 */
	public void sendChangePwdEmail(String sendTo, String from, String subject, String username, String domain,
			String changePwdUrl, String activeCode, String linkedId) throws MessagingException {
		
		//建立邮件消息
		MimeMessage mimeMessage = jms.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		//发送者
		helper.setFrom(from);
		//接收者
		helper.setTo(sendTo);
		//发送的标题
		helper.setSubject(subject);
		//发送的内容
		String text = String.format(change_pwd_template, domain, changePwdUrl, activeCode, linkedId, domain, changePwdUrl, activeCode, linkedId);
		helper.setText(text, true);
		// 发送邮件
		jms.send(mimeMessage);
		
	}	
	
}
