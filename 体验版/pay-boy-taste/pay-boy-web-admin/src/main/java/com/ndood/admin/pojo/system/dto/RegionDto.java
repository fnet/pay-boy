package com.ndood.admin.pojo.system.dto;

import java.util.List;

import com.ndood.admin.pojo.system.RegionPo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 地区DTO类
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class RegionDto extends RegionPo {
	private static final long serialVersionUID = 6650285921381454775L;
	private Integer parentId;
	
	private Integer isLeaf;

	private List<RegionDto> list;
	
}
